let plus = document.querySelector('plus');
let heart = document.querySelector('heart');
let card = document.querySelector('card');

plus.onclick = function () {
    plus.classList.toggle('plus');
    plus.classList.toggle('plus1');
};

heart.onclick = function () {
    heart.classList.toggle('heart');
    heart.classList.toggle('heart1');
};

card.onmouseover = function () {
    card.classList.add('move');
    card.classList.remove('remove');
};

card.onmouseout = function () {
    card.classList.remove('move');
    card.classList.add('remove');
};